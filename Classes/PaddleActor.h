//
//  PaddleActor.h
//  breakout
//
//  Created by 小池利幸 on 2017/06/18.
//
//

#ifndef PaddleActor_h
#define PaddleActor_h

#include "cocos2d.h"

class PaddleActor : public cocos2d::Node
{
public:
	CREATE_FUNC(PaddleActor);

	PaddleActor();
	~PaddleActor();

protected:
	cocos2d::Vec2	fieldMin_;
	cocos2d::Vec2	fieldMax_;

	void            onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	void            onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	bool            inputLeft_;		// 入力イベントの結果出力用
	bool            inputRight_;	// 入力イベントの結果出力用
	float			moveSpeed_;		// 移動スピード
	
	virtual bool init() override;
	virtual void update(float delta) override;
	virtual void onEnter() override;
	virtual void onExit() override;
};

#endif /* PaddleActor_h */
