//
//  BallActor.h
//  breakout
//
//  Created by 小池利幸 on 2017/06/18.
//
//

#ifndef BallActor_h
#define BallActor_h

#include "cocos2d.h"

class BallActor : public cocos2d::Node
{
public:
	BallActor();

	CREATE_FUNC(BallActor);
	
	static const int TAG = 1;				// このノード検索用
	bool attemptBounce(const Node* node);	// 衝突をテストして跳ね返る
	void resetPosition();
	
protected:
	cocos2d::Vec2	fieldMin_;
	cocos2d::Vec2	fieldMax_;
	cocos2d::Vec2	startPosition_;
	cocos2d::Vec2	moveDirection_;
	float			moveSpeed_;
	static const int	TAG_DRAWNODE = 100;
	float			radius_;
	cocos2d::Vec2	prevPosition_;
	
	virtual bool init() override;
	virtual void update(float delta) override;
};

#endif /* BallActor_h */
