//
//  BlockActor.h
//  breakout
//
//  Created by 小池利幸 on 2017/06/20.
//
//

#ifndef BlockActor_h
#define BlockActor_h

#include "cocos2d.h"

class BlockActor : public cocos2d::Node
{
public:
	CREATE_FUNC(BlockActor);

protected:
	virtual bool init() override;
	virtual void update(float delta) override;
	virtual void onExit() override;
};

#endif /* BlockActor_h */
