//
//  GameScene.h
//  breakout
//
//  Created by 小池利幸 on 2017/06/17.
//
//

#ifndef GameScene_h
#define GameScene_h

#include "cocos2d.h"
#include "PaddleActor.h"
#include "BallActor.h"

class GameScene : public cocos2d::Scene
{
public:
	GameScene();
	
    virtual bool init();
    CREATE_FUNC(GameScene);

	void onBlockDestroy();
	void onMiss();
	
protected:
	void changeNextScene(Scene* nextScene);
	
	PaddleActor*    paddleActor_;
	BallActor*		ballActor_;
	unsigned int	leftBlocks_;
	
	void    gameStart();
	void    gameOver();
};

#endif /* GameScene_h */
