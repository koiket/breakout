//
//  GameScene.cpp
//  breakout
//
//  Created by 小池利幸 on 2017/06/17.
//
//

#include "GameScene.h"
#include "TitleScene.h"
#include "ui/CocosGUI.h"
#include "BlockActor.h"


USING_NS_CC;

GameScene::GameScene()  : ballActor_{nullptr}, paddleActor_{nullptr}, leftBlocks_{0}
{
}


bool GameScene::init()
{
    // メソッドを処理する前に継承元の親クラスのメソッドを呼ぶ必要がある
    if (!Scene::init())
        return false;
    
    auto directorInstance = Director::getInstance();
    auto visibleSize = directorInstance->getWinSize();
    auto origin = directorInstance->getVisibleOrigin();
    
    // ラベルを作成
    auto label = Label::createWithSystemFont("Game Scene", "", 48);
    label->setPosition(Vec2(origin.x + visibleSize.width * 0.5f,
                            origin.y + visibleSize.height * 0.5f + label->getContentSize().height));
    // 自分のノードに追加
    //addChild(label, 1);

	// ボタンが押されたら呼ぶ
	auto titleButton = ui::Button::create();
	titleButton->setTitleText("→タイトル");
	titleButton->setTitleFontSize(26);
	titleButton->setPosition(Vec2(origin.x + visibleSize.width * 0.5f,
								  origin.y + visibleSize.height * 0.5f - titleButton->getContentSize().height));
	titleButton->addTouchEventListener([=](Ref* pSender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			auto nextScene = TitleScene::create();
			changeNextScene(nextScene);
		}
	});
	//addChild(titleButton);
	
	// パドルを作成
	paddleActor_ = PaddleActor::create();
	CC_ASSERT(paddleActor_);
	addChild(paddleActor_);
	
	// ボールを作成
	ballActor_ = BallActor::create();
	CC_ASSERT(ballActor_);
	addChild(ballActor_);
	ballActor_->unscheduleUpdate();	// ボールを止めとく
	
	// ブロックを作成
	auto blockFieldMin = origin;
	auto blockFieldMax = Vec2(visibleSize.width, visibleSize.height);
	blockFieldMin.y = blockFieldMax.y * 0.5f;
	auto blockFieldGap = Vec2(40.0f, 30.0f);
	auto blockFieldSize = blockFieldMax - blockFieldMin - blockFieldGap * 2.0f;
	auto blockNumberX = 14u; // 2以上
	auto blockNumberY = 6u;	// 2以上
	auto blockStepX = blockFieldSize.x / (float)(blockNumberX - 1);
	auto blockStepY = blockFieldSize.y / (float)(blockNumberY - 1);
	
	auto positionOrign = blockFieldMin + blockFieldGap;
	for (auto y = 0u; y < blockNumberY; y++)
		for (auto x = 0u; x < blockNumberX; x++) {
			auto block = BlockActor::create();
			block->setPosition(positionOrign + Vec2(blockStepX * (float)x, blockStepY * (float)y));
			addChild(block);
		}
	
	leftBlocks_ = blockNumberY * blockNumberX;
	gameStart();
    return true;
}

void GameScene::changeNextScene(Scene* nextScene)
{
	CC_ASSERT(nextScene);
	Director::getInstance()->replaceScene(nextScene);
}

void GameScene::gameStart()
{
	auto directorInstance = Director::getInstance();
	auto visibleSize = directorInstance->getWinSize();
	auto origin = directorInstance->getVisibleOrigin();
	
	// ラベル
	auto label = Label::createWithSystemFont("START", "", 48);
	CC_ASSERT(label);
	label->setColor(Color3B::GREEN);
	label->setPosition(Vec2(origin.x + visibleSize.width * 0.5f, origin.y + visibleSize.height * 0.5f + label->getContentSize().height));
	addChild(label, 1);
	
	CC_ASSERT(ballActor_);
	ballActor_->resetPosition();
	
	scheduleOnce([label, this](float deltaTime){
		label->removeFromParent();		// スタート表示を削除
		ballActor_->scheduleUpdate();	// ボールを動かす
	}, 1.0f, std::string(__FUNCTION__));
}

void GameScene::onBlockDestroy()
{
	// ブロックがなくなったらタイトルシーンへ
	if (--leftBlocks_ <= 0) {
		gameOver();
	}
}

void    GameScene::gameOver()
{
	auto directorInstance = Director::getInstance();
	auto visibleSize = directorInstance->getWinSize();
	auto origin = directorInstance->getVisibleOrigin();
	
	// ラベル
	auto label = Label::createWithSystemFont("GAME OVER", "", 48);
	CC_ASSERT(label);
	label->setPosition(Vec2(origin.x + visibleSize.width * 0.5f, origin.y + visibleSize.height * 0.5f + label->getContentSize().height));
	label->setColor(Color3B::RED);
	addChild(label, 1);
	
	CC_ASSERT(ballActor_);
	ballActor_->unscheduleUpdate();	// ボールを止める
	
	scheduleOnce([label, this](float deltaTime){
		CC_ASSERT(label);
		label->removeFromParent();
		auto nextScene = TitleScene::create();
		changeNextScene(nextScene);
	}, 1.0f, std::string(__FUNCTION__));
}

void GameScene::onMiss()
{
	auto directorInstance = Director::getInstance();
	auto visibleSize = directorInstance->getWinSize();
	auto origin = directorInstance->getVisibleOrigin();
	
	// ラベル
	auto label = Label::createWithSystemFont("MISS", "", 48);
	CC_ASSERT(label);
	label->setColor(Color3B::RED);
	label->setPosition(Vec2(origin.x + visibleSize.width * 0.5f, origin.y + visibleSize.height * 0.5f + label->getContentSize().height));
	addChild(label, 1);
	
	CC_ASSERT(ballActor_);
	ballActor_->unscheduleUpdate();	// ボールを止める
	
	scheduleOnce([label, this](float deltaTime){
		label->removeFromParent();
		gameStart();
	}, 1.0f, std::string(__FUNCTION__));
}

