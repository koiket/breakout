//
//  TitleScene.h
//  breakout
//
//  Created by 小池利幸 on 2017/06/17.
//
//

#ifndef TitleScene_h
#define TitleScene_h

class TitleScene : public cocos2d::Scene
{
public:
    virtual bool init();
    CREATE_FUNC(TitleScene);
};

#endif /* TitleScene_h */
