//
//  SplashScene.h
//  breakout
//
//  Created by 小池利幸 on 2017/06/17.
//
//

#ifndef SplashScene_h
#define SplashScene_h

#include "cocos2d.h"

class SplashScene : public cocos2d::Scene
{
public:
	virtual bool init();
	CREATE_FUNC(SplashScene);
	
protected:
	void changeNextScene(float interval);

};

#endif /* SplashScene_h */
