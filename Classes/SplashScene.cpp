//
//  SplashScene.cpp
//  breakout
//
//  Created by 小池利幸 on 2017/06/17.
//
//

#include "SplashScene.h"
#include "TitleScene.h"

USING_NS_CC;

bool SplashScene::init()
{
	// メソッドを処理する前に継承元の親クラスのメソッドを呼ぶ必要がある
	if (!Scene::init())
		return false;

	auto directorInstance = Director::getInstance();
	auto visibleSize = directorInstance->getWinSize();
	auto origin = directorInstance->getVisibleOrigin();

	// ラベルを作成
	auto label = Label::createWithSystemFont("Hinotama Games", "", 48);
	label->setPosition(Vec2(origin.x + visibleSize.width * 0.5f,
							origin.y + visibleSize.height * 0.5f + label->getContentSize().height));
	// 自分のノードに追加
	addChild(label, 1);
	
	// フェードを追加
	scheduleOnce(schedule_selector(SplashScene::changeNextScene), 3.0f);

	return true;
}

void SplashScene::changeNextScene(float interval)
{
	auto scene = TitleScene::create();
	auto fade = TransitionFade::create(0.5f, scene, Color3B::WHITE);
	Director::getInstance()->replaceScene(fade);
}
