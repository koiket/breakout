//
//  TitleScene.cpp
//  breakout
//
//  Created by 小池利幸 on 2017/06/17.
//
//

#include "TitleScene.h"
#include "GameScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

bool TitleScene::init()
{
    // メソッドを処理する前に継承元の親クラスのメソッドを呼ぶ必要がある
    if (!Scene::init())
        return false;
    
    auto directorInstance = Director::getInstance();
    auto visibleSize = directorInstance->getWinSize();
    auto origin = directorInstance->getVisibleOrigin();
    
    // ラベルを作成
    auto label = Label::createWithSystemFont("BLOCKOUT", "", 48);
    label->setPosition(Vec2(origin.x + visibleSize.width * 0.5f,
                            origin.y + visibleSize.height * 0.5f + label->getContentSize().height));
    // 自分のノードに追加
    addChild(label, 1);

	// ボタンを追加
	auto gameButton = ui::Button::create();
	gameButton->setTitleText("→ゲーム");
	gameButton->setTitleFontSize(26);
	gameButton->setPosition(Vec2(origin.x + visibleSize.width * 0.5f,
								 origin.y + visibleSize.height * 0.5f - gameButton->getContentSize().height));
	gameButton->addTouchEventListener([=](Ref* pSender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			auto nextScene = GameScene::create();
			auto transition = TransitionFade::create(1.0f, nextScene, Color3B::BLACK);
			directorInstance->replaceScene(transition);
		}
	});
	addChild(gameButton);
	
    return true;
}
