//
//  BallActor.cpp
//  breakout
//
//  Created by 小池利幸 on 2017/06/18.
//
//

#include "BallActor.h"
#include "GameScene.h"

USING_NS_CC;

BallActor::BallActor()
	:   fieldMin_{},
		fieldMax_{},
		startPosition_{},
		moveDirection_{1.0f, 1.0f},
		moveSpeed_{250.0f},
		radius_{5.f},
		prevPosition_{}
{
}

bool BallActor::init()
{
	if (!Node::init())
		return false;
	
	// 範囲と初期位置を設定
	auto directorInstance = Director::getInstance();
	CC_ASSERT(directorInstance);
	
	fieldMin_ = directorInstance->getVisibleOrigin();
	fieldMax_ = directorInstance->getWinSize();
	// 開始時の位置
	startPosition_ = (fieldMax_ - fieldMin_) * 0.5f;
	startPosition_.y = (fieldMax_.y - fieldMin_.y) * 0.4f;
	
	// サークル
	auto drawNode = DrawNode::create();
	CC_ASSERT(drawNode);
	drawNode->drawDot(Vec2::ZERO, radius_, Color4F::WHITE);
	drawNode->setTag(TAG_DRAWNODE);
	addChild(drawNode);
	setContentSize(Size(radius_ * 2.f, radius_ * 2.f));	// 後で使う用

	// 自分自身の形状の大きさぶん移動範囲を狭める
	auto contentHalfSize = getContentSize() * 0.5f;
	fieldMin_ += contentHalfSize;
	fieldMax_ -= contentHalfSize;

	scheduleUpdate();	// update を毎フレーム呼ぶ
	
	setTag(TAG);	// タグをつける

	resetPosition();
	
	return true;
}

void BallActor::update(float delta)
{
	// 移動予定位置を計算する
	auto position = getPosition();
	prevPosition_ = position;	// 前回の位置を覚えておく
	position += moveDirection_ * moveSpeed_ * delta;

	// 範囲チェックと跳ね返り
	auto reverseSwitchValue = Vec2(1.f, 1.f);
	auto minDiff = position - fieldMin_;
	auto maxDiff = position - fieldMax_;
	if (minDiff.x < 0.0f)	position.x -= minDiff.x, reverseSwitchValue.x *= -1.f;
//	if (minDiff.y < 0.0f)	position.y -= minDiff.y, reverseSwitchValue.y *= -1.f;
	if (maxDiff.x > 0.0f)	position.x -= maxDiff.x, reverseSwitchValue.x *= -1.f;
	if (maxDiff.y > 0.0f)	position.y -= maxDiff.y, reverseSwitchValue.y *= -1.f;
	moveDirection_.x *= reverseSwitchValue.x;	// 壁に当たってた場合は移動方向xを反転させる
	moveDirection_.y *= reverseSwitchValue.y;	// 壁に当たってた場合は移動方向yを反転させる

	if (minDiff.y < 0.0f)	{
		GameScene* gameScene = static_cast<GameScene*>(getScene());
		CC_ASSERT(gameScene);
		gameScene->onMiss();
	}
	
	setPosition(position);	// 位置を更新する
}

void BallActor::resetPosition()
{
	setPosition(startPosition_);
	prevPosition_ = startPosition_;
}

bool intersectionLineSegmentVsLineSegment(Vec2& position, const Vec2& p1, const Vec2& v1, const Vec2& p2, const Vec2& v2)
{
	auto det = v1.cross(v2);	// detが0の場合は為す菱形の面積がないという事イコール並行
	
	if (det == 0.0f)
		return false;
	
	auto s = 1.f / det * v1.cross(p1 - p2);
	position = v2 * s + p2;
	
	auto det2 = v2.cross(v1);
	auto s2 = 1.f / det2 * v2.cross(p2 - p1);
	
	return (s >= 0.f && s <= 1.f && s2 >= 0.f && s2 <= 1.f) ? true : false;	// s のみの範囲チェックなら、セグメント vs ラインにできる
}

Vec2 reflect(const Vec2& v, const Vec2& normal)
{
	return v - normal * Vec2::dot(v, normal) * 2.f;
}

bool BallActor::attemptBounce(const Node* node)
{
	CC_ASSERT(node);
	
	auto oppoentRectSize = node->getContentSize();
	auto oppoentRect = Rect(node->getPosition() - Vec2(oppoentRectSize) * 0.5f, oppoentRectSize);
	bool result = oppoentRect.intersectsCircle(getPosition(), radius_);
	
	auto drawNode = static_cast<DrawNode*>(getChildByTag(TAG_DRAWNODE));
	drawNode->clear();
	drawNode->drawDot(Vec2::ZERO, radius_, result ? Color4F::RED : Color4F::WHITE);
	
	if (result) {
		if (oppoentRect.intersectsCircle(prevPosition_, radius_))	// rectの中から、中もしくは外へのバウンスはしない
			return false;
		
		//  A      / -- extent
		//  +-----+B
		//  |    /|
		//  |   / |
		//  |  +  |
		//  |     |
		//  |     |
		// D+-----+
		//        C
		auto extent = Vec2(oppoentRect.size) * 0.5 + Vec2(radius_, radius_);
		auto origin = node->getPosition();
		auto edgePosition = std::vector<Vec2>{
			origin + Vec2(-extent.x,  extent.y),	// A
			origin + Vec2( extent.x,  extent.y),	// B
			origin + Vec2( extent.x, -extent.y),	// C
			origin + Vec2(-extent.x, -extent.y)		// D
		};
		auto edgeVector = std::vector<Vec2>{
			edgePosition[1] - edgePosition[0],
			edgePosition[2] - edgePosition[1],
			edgePosition[3] - edgePosition[2],
			edgePosition[0] - edgePosition[3]
		};
		static const auto edgeNormal = std::vector<Vec2>{
			Vec2( 0.f,  1.f),
			Vec2( 1.f,  0.f),
			Vec2( 0.f, -1.f),
			Vec2(-1.f,  0.f)
		};
		auto ballMoveVector = getPosition() - prevPosition_;
		for (int i = 0; i < 4; i++) {
			auto crossPosition = Vec2();
			auto isCross = intersectionLineSegmentVsLineSegment(
																crossPosition,
																prevPosition_, ballMoveVector,
																edgePosition[i], edgeVector[i]);
			if (isCross) {
				moveDirection_ = reflect(moveDirection_, edgeNormal[i]);
				auto fullDistance = ballMoveVector.length();
				auto toCrossDistance = Vec2(crossPosition - prevPosition_).length();
				auto newPosition = crossPosition + moveDirection_ * (fullDistance - toCrossDistance);
				setPosition(newPosition);
				break;	// エッジを2つ交差するのは今回考慮しない
			}
		}
	}

	return result;
}
